# OpenG

Source Code Repositories and Issue Trackers:
- [OpenG Variant Data Library](https://github.com/JKISoftware/OpenG-Variant-Data-Library)
- [OpenG Time Library](https://github.com/JKISoftware/OpenG-Time-Library)
- [OpenG File Library](https://github.com/JKISoftware/OpenG-File-Library)
- [OpenG Array Library](https://github.com/JKISoftware/OpenG-Array-Library)
- [OpenG Variant Configuration File Library](https://github.com/JKISoftware/OpenG-Variant-Configuration-File-Library)

NXG package source repository:
- [OpenG-NXG](https://github.com/JKISoftware/OpenG-NXG)
